<?php

use App\system_loss\User\source_line_meter\mpdf\mpdf;
// Require composer autoload
include_once'../../../../../vendor/autoload.php';


$obj= new mpdf();
$data = $obj->index();
$trs="";
$serial=0;
foreach($data as $item):
$serial++;
$trs.="<tr>";
$trs.="<td>".$serial."</td>";
$trs.="<td>".$item["sub_id"]."</td>";
$trs.="<td>".$item["sub_name"]."</td>";
$trs.="<td>".$item["pbs_id"]."</td>";
$trs.="</tr>";
endforeach;
$html=<<<EOD
    
    <html>
        <head>
  <litle> list of table</title>
        </head>
    <body>
    <h1>
        list of data
    </h1>
    <table border="1">
    <thead>
   
    <tr>
        <th>Sl</th>
        <th>SUb_id</th>
        <th>substation Name</th>
        <th>pbs id</th>
    </tr>
     </thead>
    <tbody>
    $trs;
    </tbody>
    </table>
    </body>
    </html>

EOD;


// Create an instance of the class:
$mpdf = new \Mpdf\Mpdf();
// Write some HTML code:
$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output();