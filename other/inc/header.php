<!DOCTYPE HTML>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title> PHP Traning</title>
  <style>
body {
  font-family: arial;
  font-size: 18px;
  line-height: 22px;
  margin: 0 auto;
  width: 900px;
}
.headeroption {
  background: darkkhaki url("img/php.png") no-repeat scroll 56px 18px;
  height: 80px;
  overflow: hidden;
  padding-left: 160px;
}
.footeroption{height:80px;background:darkkhaki;overflow:hidden;}
.headeroption h2 {
  color: #000;
  font-size: 30px;
  padding-top: 5px;
  text-shadow: 0 1px 1px #fff;
}
.footeroption h2 {
  background: rgba(0, 0, 0, 0) url("img/logo.png") no-repeat scroll 65px 0;
  color: #000;
  font-size: 30px;
  padding-bottom: 13px;
  padding-top: 10px;
  text-align: center;
  text-shadow: 1px 1px 1px #fff;
}
.content {
  background: #f2f2ff none repeat scroll 0 0;
  border: 6px solid darkkhaki;
  font-size: 16px;
  line-height: 22px;
  margin-bottom: 8px;
  margin-top: 8px;
  min-height: 420px;
  overflow: hidden;
  padding: 10px;
}
.mainleft { border-right: 1px solid #999; float: left;margin-left: 16px;width: 350px;}
.mainright{ float: right; margin-rights: 20px; width: 450px;}

.tblone{width: 100%; border: 1px solid #fff;}
.tblone th td{padding: 5px 10px;text-align: center;}

table.tblone tr:nth-child(2n+1) {background: #fff;height: 30px;}
table.tblone tr:nth-child(2n) {background: #fdf0f1;sheight: 30px;}

input[type="text"]{border: 1px solid #ddd;margin-bottom: 5px;padding: 5px;width: 228px;font-size: 16px;}
input[type="submit"]{cursor: pointer;}

.insert{color:#06960E; font-weight: bold;}
.delete{color:#DE5A24; font-weight: bold;}

  </style>
</head>
<body>
  <header class="headeroption">
    <h2>PHP Traning</h2>
  </header>
  
  <div class="content">
    <section class="subject">
      <p><h3>CRUD and DB Design<span style="float: right"><a href="Student.php">Student</a> || <a href="teacher.php">Teacher</a></span></p></h3><hr>
    </section>